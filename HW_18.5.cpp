#include <iostream>
#include<string>

template<class T>
class Stack
{
public:

    Stack(size_t size)
    {
        // Create new array
        array = new T[size];
        length = size;
    }

    ~Stack()
    {
        delete[] array;
    }

    // add new element
    void push(T element)
    {
        if (topElem == length - 1)
        {
            extendSize();
        }

        array[++topElem] = element;

    }

    // delete element
    void pop()
    {
        std::cout << "Deleted element - " << array[topElem] << '\n' << std::endl;

        topElem--;
    }

    void showStack()
    {
        if (topElem == -1)
        {
            std::cout << "Stack is Empty" << std::endl;
        }
        else
        {
            std::cout << "Stack:" << std::endl;

            for (int i = 0; i <= topElem; i++)
            {
                std::cout << array[i] << std::endl;
            }
        }
    }

private:
    const size_t addLeng = 3;
    size_t length = 0;
    int topElem = -1;
    T* array;

     void extendSize()
    {
        T* newArray = new T[length + addLeng];

        for (int i = 0; i < length; i++)
        {
            newArray[i] = array[i];
        }

        length += addLeng;

        delete[] array;

        array = newArray;
    }
};


int main()
{
    setlocale(LC_ALL, "ru");
    Stack<std::string> stack(3);

    stack.push("- �����-��, ����, ���� �� �����");
    stack.push("������, ��������� �������,");
    stack.push("�������� ������?");

    stack.push("���� ���� � ������� ������,");
    stack.push("��, �������, ��� �����!");
    stack.push("������� ������ ��� ������");
    stack.push("��� ���� ��������!");
    stack.showStack();

    std::cout << std::endl;
    stack.pop();
    stack.pop();
    stack.showStack();



    return 0;
}